#!/bin/bash
# Credit to https://github.com/cwmcbrewster/Jamf_API_Scripts/blob/master/JSS_API_update_mobileapp_versions.sh
######################### Variables #########################
jssURL=$1
apiUser=$2
apiUserPass=$3
####################### Do Not Modify #######################
# function for error reporting
abort() {
  errorString=${*}
  echo "ERROR: $errorString"
  exit 1
}

# explanation of script
if [[ ${#} -lt 1 ]] || [[ ${*} == *"-h"* ]] || [[ ${*} == *"--help"* ]]; then
  echo "Usage: $0 https://jss.example.com:8443 [username] [password]"
  echo ""
  exit 1
fi

# if jssURL is emply, warn user
if [[ -z "${jssURL}" ]]; then
  abort "Please specify a JSS server"
elif [[ `curl --connect-timeout 10 -k -sS $jssURL/healthCheck.html -w \\nStatus:\ %{http_code} | grep Status: | awk '{print $2}'` != 200 ]]; then
  abort "Could not connect to JSS server $jssURL"
fi

# collect username if not specified
if [[ -z "${apiUser}" ]]; then
    echo "JSS Username: "
    read apiUser
fi

# collect username if not specified
if [[ -z "${apiUserPass}" ]]; then
    echo "JSS Password: "
    read -s apiUserPass
fi

# test supplied details
testCredentials=$(curl --connect-timeout 10 -k -sS -u "$apiUser":"$apiUserPass" "$jssURL/JSSResource/accounts" -w \\nStatus:\ %{http_code} | grep Status: | awk '{print $2}')
if [[ "$testCredentials" == "200" ]]; then
    echo "Credentials look good, moving forward..."
else
    abort "The user account or password was wrong, or doesn't have API Rights"
fi

#check for jq
if [ ! -f /usr/local/bin/jq ]
then
  echo "jq (https://stedolan.github.io/jq/) could not be found. jq is needed to parse iTunes API results. Please install jq and try again."
  echo "If you use Homebrew, run 'brew install jq'."
  echo "Exiting..."
  exit 1
fi

#set the path for the JSS to check
JSSapiPath="${jssURL}/JSSResource/mobiledeviceapplications"

# support for xpath in Big Sur
xpath() {
    if [[ $(sw_vers -buildVersion) > "20A" ]]; then
        /usr/bin/xpath -e "$@"
    else
        /usr/bin/xpath "$@"
    fi
}

#get list of bundleIDs for JSS apps
jamfAppsXml=$(curl -s -H "Accept: text/xml" -u ${apiUser}:"${apiUserPass}" ${JSSapiPath} | xpath '//mobile_device_applications/mobile_device_application' 2>&1 | awk -F'<mobile_device_application>|</mobile_device_application>' '{print $2}' | grep .)

function check_for_updates () {

#loop through each line of the XML out put so both the bundleID and the JSS app ID of an can be worked with

while read xml_string; do

  #set the app info from the JSS
  id=$(echo "$xml_string" | awk -F'<id>|</id>' '{print $2}')
  app_bundle_id=$(echo "$xml_string" | awk -F'<bundle_id>|</bundle_id>' '{print $2}')
  jamf_version=$(echo "$xml_string" | awk -F'<version>|</version>' '{print $2}')
  itunes_lastknown_url_raw=$(curl -s -H "Accept: text/xml" -u ${apiUser}:"${apiUserPass}" ${JSSapiPath}/id/$id/subset/General | xpath '//mobile_device_application/general/itunes_store_url' 2>&1 | awk -F'<itunes_store_url>|</itunes_store_url>' '{print $2}' | grep .)
  #XML can't deal with "&". replace the escape text.
  itunes_lastknown_url="${itunes_lastknown_url_raw/&amp;/&}"
  #itunesAdamId=$(echo $itunes_lastknown_url | sed -e 's/.*\/id\(.*\)?.*/\1/')
  itunesAdamId=$(echo ${itunes_lastknown_url} | grep -o -E 'id\d{9,}' | awk -F'id' '{print $2}')

  #define itunes api lookup path with bundleID from JSS
  itunes_api_url="https://uclient-api.itunes.apple.com/WebObjects/MZStorePlatform.woa/wa/lookup?version=2&id=${itunesAdamId}&p=mdm-lockup&caller=MDM&platform=itunes&cc=us&l=en"

  #json results from itunes lookup
  itunes_data=$(curl -s -H "Accept: application/JSON" -X GET "${itunes_api_url}")
  bundleId=$(/usr/local/bin/jq -r ".results.\"${itunesAdamId}\".bundleId" <<< "${itunes_data}")
  appleVersion=$(/usr/local/bin/jq -r ".results.\"${itunesAdamId}\".offers[].version.display" <<< "${itunes_data}" 2>/dev/null)

  #check if app's bundleID matches what's on the JSS. If it's blank, there's no record on the iTunes store.
  if [[ -z $id ]]; then
    #do nothing. This is just a blank line in the data parsed from the jss
    :
  elif [[ ${app_bundle_id} != ${bundleId} ]]; then
    echo "Apple bundle ID:${app_bundle_id} | Jamf App ID:${id} | status:NO LONGER AVAILABLE"
      curl -s -H "Content-Type: text/xml" -u ${apiUser}:${apiUserPass} "${JSSapiPath}/id/${id}" -X DELETE
      echo ""
      appsNoLonger=(${appsNoLonger[@]} ${app_bundle_id})
  elif [[ ${jamf_version} != ${appleVersion} ]]; then
    echo "Apple bundle ID:${app_bundle_id} | Jamf App ID:${id} | status:VERSION MISMATCH"
    echo "Updating ${app_bundle_id} from version ${jamf_version} to version ${appleVersion}"
    appsOutOfDate=(${appsOutOfDate[@]} ${app_bundle_id})
      versionData="<mobile_device_application><general><version>${appleVersion}</version></general></mobile_device_application>"
      curl -s -H "Content-Type: text/xml" -u ${apiUser}:${apiUserPass} "${JSSapiPath}/id/${id}/subset/General" -d "${versionData}" -X PUT
      echo ""
  elif [[ -n ${id} ]]; then
    echo "Apple bundle ID:${app_bundle_id} | Jamf App ID:${id} | status:CURRENT"
  fi

done <<< "${jamfAppsXml}"

}

echo "This may take a while depending on the number of apps in your jss"
echo "Checking $(echo "${jamfAppsXml}" | wc -l | awk '{print $1}') apps..."

check_for_updates

echo ""
echo "Updated ${#appsOutOfDate[@]} apps. Deleted ${#appsNoLonger[@]} apps. All done!"
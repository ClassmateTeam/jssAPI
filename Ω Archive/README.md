# Archived Scripts

These scripts may (probably) don't work anymore as they've fallen out-of-date with the Jamf Pro API.

## deployiOSAtSchool.sh
### Synopsis

Used within the k-7 schools we work with, this will utilize a CSV with the following headers:
```
1. username     	(username)
2. full-name		(full name)
3. email			(email address)
4. apple-id         (apple id)
5. grad-year    	(grad year of student)
6. stream           (for A/B schools)
7. position     	(student || teacher || staff)
8. serial-number	(device serial number)
```
### Functions of deployiOSAtSchool.sh
* Create User Extension Attributes:
    * Grad Year
    * Stream
    * Managed Apple ID
* Create Mobile Device Extension Attributes:
    * Grad Year
    * Stream
* Update existing users with:
    * Full Name
    * Email Address
    * Managed Apple ID (EA)
    * Grad Year (EA)
    * Stream (EA)
    * Position (Student, Teacher, Staff)
* Create new users with:
    * Username
    * Full Name
    * Email Address
    * Managed Apple ID (EA)
    * Stream (EA)
    * Grad Year (EA)
    * Position (Student, Teacher, Staff)
* Update Mobile Devices with:
    * Link User to Mobile Device Inventory
    * Rename to User's Full Name
    * Update Inventory
    * Send Blank Push

#### Change Log

v1.4 - Added support for JSS 9.93+ which returns json by default
v1.3 - Added logic for 'Stream' Mobile Device & User Extension Attributes  
v1.2 - Added logic for 'Grad Year' Mobile Device Extension Attributes  
v1.1 - Added logic for 'Grad Year' & 'Managed Apple ID' User Extension Attributes  
v1.0 - Launch  

## deployUsersAtSchool.sh
### Synopsis

Used within the k-7 schools we work with, this will utilize a CSV with the following headers:
```
1. username     	(username)
2. full-name		(full name)
3. email			(email address)
4. apple-id         (apple id)
5. grad-year    	(grad year of student)
6. stream           (for A/B schools)
7. position     	(student || teacher || staff)
```
### Functions of deployUsersAtSchool.sh
* Create User Extension Attributes:
    * Grad Year
    * Stream
    * Managed Apple ID
* Update existing users with:
    * Full Name
    * Email Address
    * Managed Apple ID (EA)
    * Grad Year (EA)
    * Stream (EA)
    * Position (Student, Teacher, Staff)
* Create new users with:
    * Username
    * Full Name
    * Email Address
    * Managed Apple ID (EA)
    * Stream (EA)
    * Grad Year (EA)
    * Position (Student, Teacher, Staff)

#### Change Log

v1.0 - Launch  

## modifyMacPosition.sh
### Synopsis

Used within the k-7 schools we work with, this will utilize a CSV with the following headers:
```
1. position     	(student || teacher || staff)
2. serial-number	(device serial number)
```
### Functions of modifyMacPosition.sh
* Update Computers with:
    * Position

#### Change Log

v1.0 - Launch  

## modifyiOSPosition.sh
### Synopsis

Used within the k-7 schools we work with, this will utilize a CSV with the following headers:
```
1. position     	(student || teacher || staff)
2. serial-number	(device serial number)
```
### Functions of modifyiOSPosition.sh
* Update Mobile devices with:
    * Position

#### Change Log

v1.0 - Launch  

## modifyMacName.sh
### Synopsis

Used within the k-7 schools we work with, this will utilize a CSV with the following headers:
```
1. device-name    (computer name)
2. serial-number	(device serial number)
```
### Functions of modifyMacName.sh
* Update Computers with:
    * Name

#### Change Log

v1.0 - Launch
# Jamf Pro API Scripts

This repo contains scripts used in conjunction with the Jamf Pro API.

## modifyiOSWallpaper.sh
### Synopsis

Used within the k-7 schools we work with, this will utilize a CSV with the following headers:
```
1. serial-number    (device serial number)
2. wallpaper-path   (path to folder of images)
```
### Functions of modifyiOSWallpaper.sh
* Update Mobile devices with:
    * Lock Screen Wallpaper

### Requirements of modifyiOSWallpaper.sh
* Requires a folder with .jpeg images inside
* Each image should be labeled 01.jpeg, 02.jpeg, etc
* Each iPad should have a number in its name (iPad 01, iPad 02, etc)

#### Change Log

v2.0 - Support for Bearer Token authentication
v1.0 - Launch

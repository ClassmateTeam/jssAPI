#!/bin/bash
######################### Variables #########################
apiUser=$3
apiUserPass=$4
####################### Do Not Modify #######################
# function for error reporting
abort() {
	errorString=${*}
	echo "ERROR: ${errorString}"
	exit 1
}

# explanation of script
if [[ ${#} -lt 2 ]] || [[ ${*} == *"-h"* ]] || [[ ${*} == *"--help"* ]]; then
	echo "Usage: $0 https://jss.example.com:8443 /path/to/example.csv [username] [password]"
	echo ""
    echo "CSV should have 2 columns with the following headers:
        1. serial-number    (device serial number)        [required]
        2. wallpaper-path   (path to folder of images)    [required]"
	exit 1
fi

# set jssURL to first parameter
jssURL=$1
jssURL=${jssURL%%/}

# if jssURL is emply, warn user
if [[ -z "${jssURL}" ]]; then
	abort "Please specify a JSS server"
elif [[ `curl --connect-timeout 10 -k -sS ${jssURL}/healthCheck.html -w \\nStatus:\ %{http_code} | grep Status: | awk '{print $2}'` != 200 ]]; then
	abort "Could not connect to JSS server ${jssURL}"
fi

# set csvFile to second parameter
csvFile=$2

# if csvFile is empty, warn user
if [[ -z "${csvFile}" ]]; then
	abort "Please specify a CSV file"
fi

# if csvFile cannot be read, warn user
if [[ ! -r "${csvFile}" ]]; then
	abort "Cannot read the CSV file"
fi

# collect username if not specified
if [[ -z "${apiUser}" ]]; then
    echo "JSS Username: "
    read apiUser
fi

# collect username if not specified
if [[ -z "${apiUserPass}" ]]; then
    echo "JSS Password: "
    read -s apiUserPass
fi

# support for xpath in Big Sur
xpath() {
    if [[ $(sw_vers -buildVersion) > "20A" ]]; then
        /usr/bin/xpath -e "$@"
    else
        /usr/bin/xpath "$@"
    fi
}

# get an API token using credentials
GetAPIToken() {
	apiToken=$(curl -X POST -u "${apiUser}":"${apiUserPass}" -s "${jssURL}/api/v1/auth/token" | jq -r .token)
}

# check API token
APITokenCheck() {
	api_authentication_check=$(curl -s -H "Authorization: Bearer ${apiToken}" \
	-X GET "${jssURL}/api/v1/auth" \
	--write-out %{http_code} --output /dev/null)
}

# check and rotate API token
CheckAndRotateAPIToken() {
	APITokenCheck
	if [[ ${api_authentication_check} == 200 ]]; then
		apiToken=$(curl "${jssURL}/api/v1/auth/keep-alive" --silent --request POST --header "Authorization: Bearer ${apiToken}" | jq -r .token)
	else
		GetAPIToken
	fi
}

# invalidate API token at end
InvalidateAPIToken() {
	APITokenCheck
	if [[ ${api_authentication_check} == 200 ]]; then
		curl "${jssURL}/api/v1/auth/invalidate-token" --silent  --header "Authorization: Bearer ${apiToken}" -X POST
		apiToken=""
	fi
}

# find ID of device
findDeviceID() {
    deviceID=$(curl -k -sS "${jssURL}/JSSResource/mobiledevices/serialnumber/${serialNumber}" -H "content-type: application/xml" -H "Authorization: Bearer ${apiToken}" | xpath '/mobile_device/general/id/text()' 2>/dev/null)
}
# find number within name of device
findDeviceNumber() {
    deviceNum=$(curl -k -sS "${jssURL}/JSSResource/mobiledevices/serialnumber/${serialNumber}" -H "content-type: application/xml" -H "Authorization: Bearer ${apiToken}" | xpath '/mobile_device/general/name/text()' 2>/dev/null | sed 's/[^0-9]*//g')
}

# create base64 of wallpaper
createBaseImage() {
	wallpaperPath=${wallpaperPath%%/}
	wallpaper_image=$(base64 -i "${wallpaperPath}/${deviceNum}.jpeg")
}

# create Mobile Device Command to replace wallpaper
## wallpaper setting = 1 (Lock screen), 2 (Home screen), or 3 (Lock and home screens)
updateDeviceInfo() {
	postXML="<mobile_device_command><command>Wallpaper</command><wallpaper_setting>1</wallpaper_setting><wallpaper_content>${wallpaper_image}</wallpaper_content><mobile_devices><mobile_device><id>${deviceID}</id></mobile_device></mobile_devices></mobile_device_command>"
	curl -k -sS "${jssURL}/JSSResource/mobiledevicecommands/command/Wallpaper" -H "content-type: application/xml" -H "Authorization: Bearer ${apiToken}" -X POST -d "${postXML}" > /dev/null 2>&1
}

# test supplied details
GetAPIToken
APITokenCheck
if [[ ${api_authentication_check} == 200 ]]; then
	echo "Credentials look good, moving forward..."
else
	abort "The user account or password was wrong, or doesn't have API Rights"
fi

# remove first line of csvFile
csvFileWithoutHeader=/tmp/rename_iOS_from-tmp.csv
echo "Removing headers from CSV..."
tr -d $'\r' < "${csvFile}" | awk 'NR>1' > ${csvFileWithoutHeader}

# all the things
while IFS=, read serialNumber wallpaperPath
do
	if [[ -z ${serialNumber} ]] || [[ -z ${wallpaperPath} ]]; then
		echo "Required info missing.  Skipping... Serial Number: ${serialNumber} Wallpaper Path: ${wallpaperPath}"
	else
		echo "Processing ${serialNumber}..."
    findDeviceID
		findDeviceNumber
    	if [[ -z ${deviceID} ]]; then
        echo "Serial Number ${serialNumber} not found, skipping..."
			elif [[ -z ${deviceNum} ]]; then
				echo "Device number could not be located within Device name, skipping..."
			else
				if [[ $deviceNum == [1-9] ]]; then
					deviceNum=$(printf "%02d" ${deviceNum})
				fi
				createBaseImage
				updateDeviceInfo
        fi
    fi
done < ${csvFileWithoutHeader}
echo "Finished processing ${csvFile}"

# cleanup
rm -f ${csvFileWithoutHeader}
InvalidateAPIToken
exit 0
